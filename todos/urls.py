from django.urls import path
from todos.views import todo_lists, show_list

urlpatterns = [
    path("", todo_lists, name="todo_lists"),
    path("<int:id>/", show_list, name="show_list"),
]
