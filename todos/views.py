from django.shortcuts import render, redirect
from todos.models import TodoList
from todos.forms import TodosForm
from django.shortcuts import get_object_or_404


# Create your views here.
def create_todo_list(request):
    if request.method == "POST":
        form = TodosForm(request.POST)
        if form.is_valid():
            todo_lists = form.save(False)
            todo_lists.author = request.user
            todo_lists.save()
            return redirect("list")
    else:
        form = TodosForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_lists(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_lists": todo_list,
    }
    return render(request, "todos/list.html", context)


def show_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list": list,
    }
    return render(request, "todos/detail.html", context)
